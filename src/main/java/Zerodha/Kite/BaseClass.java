package Zerodha.Kite;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

public class BaseClass {
	public WebDriver driver;
	public static Logger log = LogManager.getLogger(BaseClass.class.getName());

	// fetching test par ameters from mysql database

	public WebDriver initDriver(String[] data) throws IOException, FileNotFoundException, ClassNotFoundException, SQLException, ParseException {

		String mother_tongue = data[4];
		String url = data[5];

           System.out.println("Test initiated for below parameters from json");
		for (String a:data ) {
			System.out.println(a);
            }

		if (mother_tongue.equalsIgnoreCase("marathi")) {
			// System.out.println("Test initiated using chrome browser");
			System.out.println("Test initiated using community  "+mother_tongue);
			System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
			// System.out.println(url);
			driver.get(url);
			log.info(driver.getTitle());

		}
		if (mother_tongue.equalsIgnoreCase("gujrati")) {
			// System.out.println("Test initiated using chrome browser");
			System.out.println("Test initiated using community  "+mother_tongue);
			System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
			// System.out.println(url);
			driver.get(url);
			log.info(driver.getTitle());

		}


		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}


	public void screenshot(String testCaseName, WebDriver driver) throws IOException {

		TakesScreenshot ts = (TakesScreenshot) driver; // taking screenshot with help of driver
		File source = ts.getScreenshotAs(OutputType.FILE); // saving screenshot to a File
		String destinationFile = System.getProperty("user.dir") + "\\Reports\\" + testCaseName + ".png"; //creating aPNG filewith dynamic methodname and webdriverinstance

		FileUtils.copyFile(source, new File(destinationFile)); // Transferring PNG file from source to Destination
																// location in project folder
		System.out.println("Ereor screenshot attached with " + testCaseName);
	}

}