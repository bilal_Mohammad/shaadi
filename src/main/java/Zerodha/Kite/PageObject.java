package Zerodha.Kite;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PageObject extends BaseClass {

	//Landing page Elements

	By letsBegin = By.xpath("//div[@class='buttonWrap']");
	By signUp = By.xpath("form-module--signUpFree--gxEYA");
	By email = By.name("email");
	By password = By.name("password1");
	By drop = By.xpath("//div[@class='Dropdown-control postedby_selector']");
	By next =By.xpath("//button[@class='btn btn-primary btn-md btn-block']");
	By community = By.xpath("(//div[@class='Dropdown-control mother_tongue_selector Dropdown-disabled']//div)[1]");



	public PageObject(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;  // assigning driver instance from Launch class to local instance
	}
	//returning elements
	public WebElement letsBegin() {

		return  driver.findElement(letsBegin);

	}
	public WebElement signUp() {

		return  driver.findElement(signUp);

	}
	public WebElement email() {

		return  driver.findElement(email);

	}
	public WebElement password() {

		return  driver.findElement(password);

	}
	public WebElement drop() {

		return  driver.findElement(drop);

	}
	public WebElement next() {

		return  driver.findElement(next);

	}
	public WebElement community() {

		return  driver.findElement(community);

	}

}
