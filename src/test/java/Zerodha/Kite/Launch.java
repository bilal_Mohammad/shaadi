package Zerodha.Kite;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class Launch extends BaseClass {

	 @Test(dataProvider = "dp")
	public void launching_marathi_shaadi(String data) throws FileNotFoundException, ClassNotFoundException, IOException, SQLException, ParseException {
		 WebDriver driver;

		 String expectedTitle = "Marathi Matrimony & Matrimonial site - MarathiShaadi.com";
		 String title ;
		 //reading data from data provider json
		 String creds[] = data.split(",");  //splitting elements inside array by ","
		 driver = initDriver(creds); //Initiate the driver
		title = driver.getTitle();
        Assert.assertEquals(expectedTitle, title);
		System.out.println("user is on correct domain" + title);
		 PageObject obj = new PageObject(driver);    //Create Page Object reference

		 obj.letsBegin().click();
		 System.out.println("lets begin clicked");
		 obj.email().sendKeys(creds[0]);
		 System.out.println("email entered");
		 obj.password().sendKeys(creds[1]);
		 System.out.println("password entered");
		 obj.drop().click();
		 System.out.println("dropdwon clicked");

		 //fetching all the dropdwon values
		 List<WebElement> dropOption = driver.findElements(By.xpath("//div[@class='Dropdown-option']"));
		 droploop:
		 for (int i = 0; i < dropOption.size(); i++) {

			 String dropData = dropOption.get(i).getText();
			 System.out.println(dropData);
			 int j = i + 1;

			 if (dropData.equalsIgnoreCase(creds[2])) {
				 System.out.println("creator input matches" + " " + creds[2] + "matched with" + "  dropdwon" + dropData);
				 driver.findElement(By.xpath("(//div[@class='Dropdown-option'])['+j+']")).click();
				 System.out.println("creator values selected");
				 break droploop;
			 } else {
				 System.out.println("dropdwon didnt match input value of" + creds[2]);
			 }


		 }
		 //store gender options to list
		 List<WebElement> gender = driver.findElements(By.xpath("//div[@class='form-check form-check-inline']//label"));
		 for (WebElement option : gender) {
			 if (option.getText().equalsIgnoreCase(creds[3])) //comparing gender input from data json gneder input
				 option.click();
			 System.out.println(option.getText() + "   chosen as gender");
			 break;
		 }


			obj.next().click(); //user navigates to panel 2
			System.out.println("next clicked");

			String expected_commumity [] = title.split(" ");
			String trimmed_titile =  expected_commumity[0];
			String commtext = obj.community().getText();
			Assert.assertEquals(trimmed_titile,commtext); //mother tongue assertion
			System.out.println("community matches mother tongue");


	}

	@DataProvider(name="dp")
	public Object[] readJson() throws IOException, ParseException {

		JSONParser jsonParser = new JSONParser();
		FileReader read = new FileReader(".\\resources\\data.json");
		Object obj = jsonParser.parse(read);
		JSONObject userData  = (JSONObject)obj; //typecasting java object to json object
	          //getting values from json
		JSONArray values =  (JSONArray)userData.get("testData"); //getting json array from the json object

		String inputArray[] = new String[values.size()];
		//  for (Object  a:values) {
		for (int i=0; i< values.size();i++){
			JSONObject creds = (JSONObject) values.get(i);
		String userName = (String) creds.get("userName");
			String pwd = (String)creds.get("password");
			String creator = (String) creds.get("profileCreator");
			String gender = (String) creds.get("gender");
			String  mother_tongue = (String) creds.get("mother_tongue");
			String  url = (String) creds.get("url");
			String  browser = (String) creds.get("browser");
			inputArray[i] =userName+","+pwd+","+creator+","+gender+","+mother_tongue+","+url+","+browser;
		}
		return inputArray;

	}



}
